## jfinal-undertow

[中文](README.md) [English](README_en.md)

#### Project Introduction
jfinal-undertow is used for the development and deployment of web projects developed by jfinal. It innovatively introduces the HotSwapClassLoader + HotSwapWatcher, achieving hot-reloading for both development and deployment with just 321 lines of code. This approach is unprecedented, and will surely inspire others to follow suit.

## I. Quick Start
### 1: Add maven dependency
```
<dependency>
	<groupId>com.jfinal</groupId>
	<artifactId>jfinal-undertow</artifactId>
	<version>3.7</version>
</dependency>
```

Note: Remove the previous maven dependency on jetty-server.

### 2: Create a main method and launch the project in eclipse or IDEA.
```
UndertowServer.start(AppConfig.class);
```

Here, AppConfig is a subclass inherited from JFinalConfig. The previous JFinal.start(...) usage is no longer needed.

## II. Quick Packaging and Deployment
### 1: Change the packaging type in the header of pom.xml from war to jar.
```
<packaging>jar</packaging> 
```

### 2: Add maven-jar-plugin to pom.xml.
```
<!--
    jar 包中的配置文件优先级高于 config 目录下的 "同名文件"
    因此，打包时需要排除掉 jar 包中来自 src/main/resources 目录的
    配置文件，否则部署时 config 目录中的同名配置文件不会生效
-->
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-jar-plugin</artifactId>
	<version>2.6</version>
	<configuration>
		<excludes>
			<!-- 
				*.* 用法，可以匹配 jar 包根目录下所有文件
				*.xxx 用法，可以匹配 jar 包根目录下特定扩展名文件，例如：*.xml
				**/* 前缀用法，可以匹配所有路径，例如：**/*.txt
			-->
			<exclude>*.*</exclude>
		</excludes>
	</configuration>
</plugin>
```
This plugin is solely to prevent configuration files from being included in the jar. If packaging as a fatjar, this plugin is unnecessary.

### 3: Add maven-assembly-plugin to pom.xml.
```
<plugin>
	<groupId>org.apache.maven.plugins</groupId>
	<artifactId>maven-assembly-plugin</artifactId>
	<version>3.1.0</version>
	<executions>
		<execution>
			<id>make-assembly</id>
			<phase>package</phase>
			<goals>
				<goal>single</goal>
			</goals>

			<configuration>
				<!-- 打包生成的文件名 -->
				<finalName>${project.artifactId}</finalName>
				<!-- jar 等压缩文件在被打包进入 zip、tar.gz 时是否压缩，设置为 false 可加快打包速度 -->
				<recompressZippedFiles>false</recompressZippedFiles>
				<!-- 打包生成的文件是否要追加 release.xml 中定义的 id 值 -->
				<appendAssemblyId>true</appendAssemblyId>
				<!-- 指向打包描述文件 package.xml -->
				<descriptors>
					<descriptor>package.xml</descriptor>
				</descriptors>
				<!-- 打包结果输出的基础目录 -->
				<outputDirectory>${project.build.directory}/</outputDirectory>
			</configuration>
		</execution>
	</executions>
</plugin>
```

### 4: Add a packaging descriptor file, package.xml, to the project root. Contents are as follows:
```
<assembly xmlns="http://maven.apache.org/ASSEMBLY/2.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/ASSEMBLY/2.0.0 http://maven.apache.org/xsd/assembly-2.0.0.xsd">
	<!--
		assembly 打包配置更多配置可参考官方文档：
		http://maven.apache.org/plugins/maven-assembly-plugin/assembly.html
	-->

	<id>release</id>

	<!--
		设置打包格式，可同时设置多种格式，常用格式有：dir、zip、tar、tar.gz
		dir 格式便于在本地测试打包结果
		zip 格式便于 windows 系统下解压运行
		tar、tar.gz 格式便于 linux 系统下解压运行
	-->
	<formats>
		<format>dir</format>
		<format>zip</format>
		<!-- <format>tar.gz</format> -->
	</formats>

	<!-- 打 zip 设置为 true 时，会在 zip 包中生成一个根目录，打 dir 时设置为 false 少层目录 -->
	<includeBaseDirectory>true</includeBaseDirectory>

	<fileSets>
		<!-- src/main/resources 全部 copy 到 config 目录下 -->
		<fileSet>
			<directory>${basedir}/src/main/resources</directory>
			<outputDirectory>config</outputDirectory>
		</fileSet>

		<!-- src/main/webapp 全部 copy 到 webapp 目录下 -->
		<fileSet>
			<directory>${basedir}/src/main/webapp</directory>
			<outputDirectory>webapp</outputDirectory>
			<excludes>
				<!-- **/* 前缀用法，可以匹配所有路径，例如：**/*.txt -->
				<exclude>WEB-INF</exclude>
				<exclude>WEB-INF/web.xml</exclude>
			</excludes>
		</fileSet>

		<!-- 项目根下面的脚本文件 copy 到根目录下 -->
		<fileSet>
			<directory>${basedir}</directory>
			<outputDirectory></outputDirectory>
			<!-- 脚本文件在 linux 下的权限设为 755，无需 chmod 可直接运行 -->
			<fileMode>755</fileMode>
			<lineEnding>unix</lineEnding>
			<includes>
				<include>*.sh</include>
			</includes>
		</fileSet>
		<fileSet>
			<directory>${basedir}</directory>
			<outputDirectory></outputDirectory>
			<fileMode>755</fileMode>
			<lineEnding>windows</lineEnding>
			<includes>
				<include>*.bat</include>
			</includes>
		</fileSet>


		<!-- 项目 lib 目录下的本地 jar 包全部 copy 到 lib 目录下 -->
		<!-- fileSet>
			<directory>${basedir}/lib</directory>
			<outputDirectory>lib</outputDirectory>
		</fileSet -->
	</fileSets>

	<!-- 依赖的 jar 包 copy 到 lib 目录下 -->
	<dependencySets>
		<dependencySet>
			<outputDirectory>lib</outputDirectory>
		</dependencySet>
	</dependencySets>
</assembly>
```

### 5: Add project run script files to the project root directory.
This project provides jfinal.sh and jfinal.bat scripts in the root directory. jfinal.sh is for Linux/Mac systems, while jfinal.bat is for Windows. Ensure to modify the MAIN_CLASS variable in these scripts to point to your project entry, e.g.:
```
MAIN_CLASS=com.yourpackage.YourMainClass
```
These script files are optional. You can write your own startup scripts based on personal preference.

### 6: Run the packaging command in the terminal.
```
mvn clean package
```

### 7: Deployment
Navigate to the project's target/your-project-release directory and run ./jfinal.sh start to launch the project. The target directory will also contain a your-project-release.zip file. This is a compressed zip file of the directory generated in the fifth step. Upload and extract this file on the server to deploy. You can modify the package.xml to change the generated filename or opt not to generate this file.

## III. Advantages of jfinal-undertow:
1: Ultra-fast startup, 5 to 8 times faster than Tomcat. The jfinal.com official website starts within 1.5 seconds.
2: A brilliantly simple hot-deployment design ensures lightning-fast and lightweight hot-reloading, enhancing the development experience.
3: Performance surpasses Tomcat and Jetty, suitable as a replacement for production environments.
4: Undertow is designed for embedding, allowing direct deployment in production environments without downloading or configuring services, making it ideal for microservices development and deployment.
5: Say goodbye to web.xml, Tomcat, and Jetty, saving a significant amount of packaging and deployment time. It makes development, packaging, and deployment a joy.
6: Feature-rich, supporting classHotSwap, WebSocket, gzip compression, servlets, filters, sessionHotSwap, and more.
7: Supports both fatjar and non-fatjar packaging modes, preparing jfinal for upcoming microservices functionality.
8: Unified development, packaging, and deployment processes. The entire workflow requires no adjustments or modifications to any part of the project, truly achieving rapid development to rapid deployment.
9: The above are just some of the features of jfinal-undertow. For more useful functionalities like the fatjar packaging mode, please refer to the official jfinal documentation.